import { dayForecast } from "../DayForecastStateStore";

export const useStateStores = () => {
    return dayForecast;
};