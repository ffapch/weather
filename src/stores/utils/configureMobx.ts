import { configure } from 'mobx';

const configureMobx = (): void =>
    configure({
        enforceActions: 'always',
        computedRequiresReaction: true,
        observableRequiresReaction: true,
        reactionRequiresObservable: true
    });

export default configureMobx;