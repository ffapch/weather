import { makeAutoObservable } from 'mobx';
import { api } from '../api';
import { toLocaleDateString } from '../helpers/formatDate';
import { IDayForecast, FilterValues, FilterState } from '../types';

export enum FetchForecastState {
    Pending,
    Done,
    Error
}

export class DayForecastStore {
    selectedDay: IDayForecast | null = null;    
    days: IDayForecast[] = [];
    fetchState: FetchForecastState = FetchForecastState.Pending;
    filterValues: FilterValues = {...this.defaultFilter()};

    constructor() {        
        makeAutoObservable(this);
    }

    defaultFilter(): FilterValues  {
        return {
            minTemperature: "",
            maxTemperature: "",
            weatherTypes: [],
            filterState: FilterState.Empty
        }
    }

    updateFilter(filterValues: FilterValues) {
        this.filterValues = filterValues;

        if (this.filterValues.filterState == FilterState.Filtered) {

            const minTemperatureNumber = parseInt(this.filterValues.minTemperature);
            const maxTemperatureNumber = parseInt(this.filterValues.maxTemperature);

            const minTemperatureFilter = minTemperatureNumber ? minTemperatureNumber : Number.MIN_SAFE_INTEGER;
            const maxTemperatureFilter = maxTemperatureNumber ? maxTemperatureNumber : Number.MAX_SAFE_INTEGER;

            const filteredDays = this.days.filter(day =>
                day.temperature >= minTemperatureFilter
                && day.temperature <= maxTemperatureFilter
                && (this.filterValues.weatherTypes && this.filterValues.weatherTypes.length > 0 ? this.filterValues.weatherTypes.includes(day.type) : true));

            this.days = [];

            this.days.push(...filteredDays);

            this.findSelectedDay();
        }
        else if (this.filterValues.filterState == FilterState.Empty) {
            this.fetchForecasts();
        }
    }

    isFilteredDaysExist() {
        return dayForecastStore.filterValues.filterState == FilterState.Empty
            || (dayForecastStore.filterValues.filterState == FilterState.Filtered
                && dayForecastStore.days.length > 0);
    }

    setSelectedDay(dayForecast: IDayForecast) {
        this.selectedDay = { ...dayForecast };
    }

    isSelectedDay(day: number) {
        return this.selectedDay?.day === day;
    }

    findSelectedDay() {
        const currentDate = new Date();
        const initialDay = this.days.find(x => toLocaleDateString(x.day) === toLocaleDateString(currentDate));

        if (initialDay) {
            this.setSelectedDay(initialDay);
        }
        else {
            if (this.days.length > 0) {
                this.setSelectedDay(this.days[0]);
            }
            else {
                this.selectedDay = null;
            }
        }
    }

    async fetchForecasts() {
        this.days = [];

        try {
            const forecasts = await api.getWeather();

            this.days.push(...forecasts);

            this.findSelectedDay();

            this.fetchState = FetchForecastState.Done;
        }
        catch (error) {
            console.error("Failed to fetch forecasts", error);
            this.fetchState = FetchForecastState.Error;
        }

        return this.days;
    }
}

export const dayForecastStore = new DayForecastStore();