import { flow, types } from 'mobx-state-tree';
import { api } from '../api';
import { toLocaleDateString } from '../helpers/formatDate';

export enum FetchForecastState {
    Pending,
    Done,
    Error
}

const Filter = types
    .model('Filter', {
        minTemperature: types.string,
        maxTemperature: types.string,
        weatherType: types.array(types.string)
    });

const DayForecast = types
    .model('DayForecast', {
        id: types.string,
        rain_probability: types.number,
        humidity: types.number,
        day: types.number,
        temperature: types.number,
        type: types.string
    });

const DayForecasts = types
    .model('DayForecasts', {
        days: types.array(DayForecast),
        selectedDayId: types.string,
        state: FetchForecastState.Pending,
        filter: Filter
    })
    .actions(self => {
        const actions = {
            fetchForecasts: flow(function* () {
                self.days.clear();

                try {
                    const forecasts = yield api.getWeather();
                    self.days.push(...forecasts);

                    const currentDate = new Date();
                    const initialDay = self.days.find(x => toLocaleDateString(x.day) === toLocaleDateString(currentDate));

                    if (initialDay) {
                        actions.setSelectedDayId(initialDay.id);
                    }

                    self.state = FetchForecastState.Done;
                }
                catch (error) {
                    console.error("Failed to fetch forecasts", error);
                    self.state = FetchForecastState.Error;
                }

                return self.days;
            }),
            setSelectedDayId(id: string) {
                self.selectedDayId = id;
            },
            filterDays(minTemperature: number | undefined, maxTemperature: number | undefined, weatherTypes: Array<string> | undefined) {
                const minTemperatureFilter = minTemperature ?? Number.MIN_SAFE_INTEGER;
                const maxTemperatureFilter = maxTemperature ?? Number.MAX_SAFE_INTEGER;

                const filteredForecasts = self.days.filter(day =>
                    day.temperature >= minTemperatureFilter
                    && day.temperature <= maxTemperatureFilter
                    && (weatherTypes && weatherTypes.length > 0 ? weatherTypes.includes(day.type) : true));

                console.log("filteredForecasts:", filteredForecasts);

                self.days.clear();
                
                self.days.push(...filteredForecasts);

                return self.days;
            }
        };

        return actions;
    })
    .views(self => {
        const views = {
            get Days() {
                return self.days;
            },
            get selectedDay() {
                return self.days.find(x => x.id == self.selectedDayId);
            },
            isSelectedDay(id: string) {
                return self.selectedDayId == id;
            }
        };

        return views;
    });


export const dayForecast = DayForecasts.create({
    days: [],
    selectedDayId: "",
    state: FetchForecastState.Pending,
    filter: {
        minTemperature: "",
        maxTemperature: "",
        weatherType: []
    }
});