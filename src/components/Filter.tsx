import { Formik } from "formik";
import { InputField, CheckboxField, ButtonField } from "../fields";
import { useState } from "../stores/utils";
import { FilterState, FilterValues, WeatherType } from "../types";
import { observer } from "mobx-react-lite";

export const Filter = observer(() => {
    const dayForecastStore = useState();

    return (
        <Formik
            initialValues={dayForecastStore.filterValues}            
            onSubmit={(values: FilterValues) => {                
                dayForecastStore.updateFilter(values);
            }}>
            {(props) => (
                <form onSubmit={props.handleSubmit}>
                    <div className="filter">
                        <CheckboxField label="Облачно" name="weatherTypes" value={WeatherType[WeatherType.Cloudy].toLowerCase()} />
                        <CheckboxField label="Солнечно" name="weatherTypes" value={WeatherType[WeatherType.Sunny].toLowerCase()} />
                        <p className="custom-input">
                            <InputField label="Минимальная температура" type='number' name="minTemperature" />
                        </p>
                        <p className="custom-input">
                            <InputField label="Максимальная температура" type='number' name="maxTemperature" />
                        </p>
                        <ButtonField type='submit' name="filterState" value={FilterState.Empty} />
                    </div>
                </form>
            )}
        </Formik>
    );
});