import { observer } from 'mobx-react-lite';
import { useEffect } from 'react';
import { FetchForecastState } from '../stores/DayForecastStateStore';
import { useState } from '../stores/utils';
import { FilterState } from '../types';
import { Day } from "./Day";

export const Forecast = observer(() => {
    const dayForecastStore = useState();

    useEffect(() => {
        dayForecastStore.fetchForecasts();
    }, []);   

    const daysJSX = dayForecastStore.days.map(dayForecast => {
        return <Day key={dayForecast.id} dayForecast={dayForecast} />;
    });

    return (
        <div className="forecast">
            {dayForecastStore.fetchState == FetchForecastState.Error && <div>error: Something went wrong...</div>}
            {dayForecastStore.fetchState == FetchForecastState.Pending && (<div>Loading ...</div>)}
            {!dayForecastStore.isFilteredDaysExist() ? (<p className="message">По заданным критериям нет доступных дней!</p>) : daysJSX}
        </div>
    )
})