import { observer } from "mobx-react-lite";
import { getDayName, getDate } from "../helpers/formatDate";
import { useState } from "../stores/utils";

export const Head = observer(() => {
    const dayForecastStore = useState();

    const headJSX = <div className="head">
        <div className="icon cloudy"></div>
        <div className="current-date">
            <p>{getDayName(dayForecastStore.selectedDay?.day ?? new Date().getTime())}</p>
            <span>{getDate(dayForecastStore.selectedDay?.day ?? new Date().getTime())}</span>
        </div>
    </div>

    const returnJSX = dayForecastStore.isFilteredDaysExist() ? headJSX : null;

    return (returnJSX)
})