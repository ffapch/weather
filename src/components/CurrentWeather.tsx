import { observer } from "mobx-react-lite";
import { useState } from "../stores/utils";

export const CurrentWeather = observer(() => {
    const dayForecastStore = useState();

    const currentWeatherJSX =
        <div className="current-weather">
            <p className="temperature">{dayForecastStore.selectedDay?.temperature ?? 0}</p>
            <p className="meta">
                <span className="rainy">%{dayForecastStore.selectedDay?.rain_probability ?? 0}</span>
                <span className="humidity">%{dayForecastStore.selectedDay?.humidity ?? 0}</span>
            </p>
        </div>

    const returnJSX = dayForecastStore.isFilteredDaysExist() ? currentWeatherJSX : null;
    
    return (returnJSX)
})