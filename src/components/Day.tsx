import { observer } from "mobx-react-lite";
import { FC } from "react";
import { getDayName } from "../helpers/formatDate"
import { useState } from "../stores/utils";
import { IDayForecast } from '../types/DayForecast';

interface IDayForecastProps {
    dayForecast: IDayForecast
}

export const Day: FC<IDayForecastProps> = observer(({ dayForecast }) => {
    const dayForecastStore = useState();
    
    return (
        <div className={`day ${dayForecast.type} ${dayForecastStore.isSelectedDay(dayForecast.day) ? 'selected' : ''}`}
            onClick={() => dayForecastStore.setSelectedDay(dayForecast)}
        >
            <p>{getDayName(dayForecast.day)}</p>
            <span>{dayForecast.temperature}</span>
        </div>
    )
})