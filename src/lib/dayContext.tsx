import { createContext, SetStateAction, useState, ReactElement, FC } from "react";
import { IDayForecast } from "../types/DayForecast";

type SelectedDayProviderPropTypes = {    
    children: ReactElement | ReactElement[];
}

type SelectedDayProvideShape = [
    IDayForecast | null,
    React.Dispatch<SetStateAction<IDayForecast | null>>,
];

export const DayContext = createContext<SelectedDayProvideShape>([
    null,
    () => null,
]);

export const SelectedDayProvider: FC<SelectedDayProviderPropTypes> = ({ children }) => {
    const state = useState<IDayForecast | null>(null);

    return <DayContext.Provider value={state}>{children}</DayContext.Provider>;
};
