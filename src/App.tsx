import { QueryClientProvider } from 'react-query';
import { CurrentWeather } from './components/CurrentWeather';
import { Filter } from './components/Filter';
import { Head } from './components/Head';
import { Forecast } from './components/Forecast';
import { queryClient } from './hooks/react-query/queryClient';

export const App: React.FC = () => {
    return (
        <main>
            <QueryClientProvider client={queryClient}>
                <Filter />
                <Head />
                <CurrentWeather />
                <Forecast />
            </QueryClientProvider>
        </main>
    );
};
