import { useQuery } from 'react-query';
import { api } from '../../api';
import { IDayForecast } from '../../types/DayForecast';

export const useDayForecast = () => {
    const query = useQuery<IDayForecast[], Error>('forecasts', () => api.getWeather());

    return query;
};