export enum WeatherType {
    Sunny,
    Rainy,
    Cloudy
}

export enum FilterState {
    Empty,
    Set,
    Filtered
}

export interface FilterValues {
    minTemperature: string;
    maxTemperature: string;
    weatherTypes: Array<string>;
    filterState: FilterState;
}