export interface IDayForecast {
    id: string;
    rain_probability: number;
    humidity: number;
    day: number;
    temperature: number;
    type: string
}