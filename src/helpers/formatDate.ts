export const getDayName = (day: number) => {
    return new Date(day).toLocaleDateString('ru-RU', { weekday: 'long' });
};

export const getDate = (day: number) => {
    return new Date(day).toLocaleDateString('ru-RU', { month: 'long', day: 'numeric' });
};

export const toLocaleDateString = (day: number | Date) => {
    return new Date(day).toLocaleDateString('ru-RU', { month: 'numeric', day: 'numeric', year: 'numeric' });
};