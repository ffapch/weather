/* Core */
import waait from 'waait';
import { IDayForecast } from "../types/DayForecast";
import axios from "axios";
import { numberDaysOfWeek } from '../constants';

const WEATHER_API_URL = process.env.REACT_APP_WEATHER_API_URL;

interface IForecastData {
    data: IDayForecast[]
}

export const api = {
    async getWeather(maxNumber: number = numberDaysOfWeek) {
        await waait(1000);

        const { data: { data: forecasts }, status, } = await axios.get<IForecastData>(`${WEATHER_API_URL}/?limit=${maxNumber}`);

        if (status != 200) {
            throw new Error("Could not get forecast.");
        }

        console.log(`${maxNumber} results: `, forecasts);        
        return forecasts;
    },
};
