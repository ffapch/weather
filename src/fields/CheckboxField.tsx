import { FieldHookConfig, useField, useFormikContext } from 'formik';
import { FC } from 'react';
import { FilterState, FilterValues } from '../types';

type CheckboxFieldProps = { label: string } & FieldHookConfig<string[]>;

export const CheckboxField: FC<CheckboxFieldProps> = ({ label, ...props }) => {
    const { values, setFieldValue } = useFormikContext<FilterValues>();
    const [field, meta, helpers] = useField(props);
    
    return (
        <span
            className={`checkbox ${field.value.includes(props.value) ? 'selected' : ''}`}
            {...props}
            onClick={() => {
                if(values.filterState == FilterState.Filtered) {
                    return false;
                }

                if (!field.value.includes(props.value)) {
                    helpers.setValue([props.value]);
                    setFieldValue("filterState", FilterState.Set);
                }
            }}
        >
            {label}
        </span>
    );
};