import { useField, FieldHookConfig, useFormikContext } from 'formik';
import { FC } from 'react';
import { FilterState, FilterValues } from '../types';

type InputFieldProps = { label: string } & FieldHookConfig<string>;

export const InputField: FC<InputFieldProps> = ({ label, ...props }) => {
    const { values, setFieldValue } = useFormikContext<FilterValues>();
    const [field] = useField(props);

    return (
        <>
            <label htmlFor={props.name}>{label}</label>
            <input
                {...field}
                type={props.type}
                id={props.name}
                readOnly={ values.filterState == FilterState.Filtered }
                onChangeCapture={() => {
                    setFieldValue("filterState", FilterState.Set);
                }}
            />
        </>
    );
};