import { useField, FieldHookConfig, useFormikContext } from 'formik';
import { FC } from 'react';
import { FilterState, FilterValues } from '../types';

type ButtonFieldProps = {} & FieldHookConfig<string>;

export const ButtonField: FC<ButtonFieldProps> = ({ ...props }) => {
    const { values, setFieldValue } = useFormikContext<FilterValues>();
    const [field] = useField(props);

    return (
        <>
            <button
                disabled={values.filterState == FilterState.Empty}
                {...field}
                type='submit'
                onClick={() => {
                    if (values.filterState == FilterState.Set) {
                        setFieldValue("filterState", FilterState.Filtered);
                    }

                    if (values.filterState == FilterState.Filtered) {
                        setFieldValue("minTemperature", "");
                        setFieldValue("maxTemperature", "");
                        setFieldValue("weatherTypes", Array<string>());
                        setFieldValue("filterState", FilterState.Empty);
                    }
                }}
            >
                {values.filterState === FilterState.Filtered ? 'Сбросить' : 'Отфильтровать'}
            </button>
        </>
    );
};